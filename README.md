# SKIN-Energy

## Simulating Knowledge Dynamics in Innovation Networks

SKIN (Simulating Knowledge Dynamics in Innovation Networks) is a multi-agent
model of innovation networks in knowledge-intensive industries grounded in
empirical research and theoretical frameworks from innovation economics and
economic sociology. The agents represent innovative firms who try to sell their
innovations to other agents and end users but who also have to buy raw
materials or more sophisticated inputs from other agents (or material
suppliers) in order to produce their outputs. This basic model of a market is
extended with a representation of the knowledge dynamics in and between the
firms. Each firm tries to improve its innovation performance and its sales by
improving its knowledge base through adaptation to user needs, incremental or
radical learning, and co-operation and networking with other agents.

This version of the SKIN model is called **SKIN-Energy**. It is based on the
[original SKIN model](https://github.com/InnovationNetworks/skin), which is
called **Basic SKIN** in this context. The *HOW IT WORKS* section of
`skin.nlogo` (displayed in the "Info" tab when opened with NetLogo) contains
more details.

To run this model, open `skin.nlogo` with NetLogo. You will also need to
install the NetLogo extension `hashing` that you can find
[here](https://bitbucket.org/iqib/netlogo-hashing).

Get NetLogo here: <http://ccl.northwestern.edu/netlogo/oldversions.shtml>.

This model was developed and operated with NetLogo version 6.1.0, but seems
to run fine (and a bit faster) with NetLogo 6.2.0.

## Specifics of SKIN-Energy for the project InnoSEn

This version of SKIN-Energy is a modification of the base SKIN for the research
project
[InnoSEn](https://www.iqib.de/de/verbundprojekt-netzwerkanalyse-und-simulation-von-innovationsdynamiken-neuer.html).
SKIN-Energy first emerged in the course of the InnoSEn project, so this SKIN
version was the first version of SKIN-Energy.

## CREDITS

To cite the SKIN-Energy model please use the following acknowledgement together
with the reference to the basic SKIN model:

Droste-Franke, Bert, Voge, Markus, van Doren, Davy (2020) The model
SKIN-Energy. IQIB GmbH, Bad Neuenahr-Ahrweiler

Copyright 2020 for SKIN-Energy parts: Bert Droste-Franke, Markus Voge, Davy van
Doren. IQIB GmbH. All rights reserved.

This code is an adaptation of the Basic and EIS SKIN model to the study scope
of

> "Netzwerkanalyse und Simulation von Innovationsdynamiken neuer
> Schlüsseltechnologien im Energiebereich (InnoSEn)"

funded by the German Federal Ministry for Economic Affairs and Energy, FKZ
03ET4032A.

To cite the basic SKIN model please use the following acknowledgement:

Gilbert, Nigel, Ahrweiler, Petra and Pyka, Andreas (2010) The SKIN (Simulating
Knowledge Dynamics in Innovation Networks) model.  University of Surrey,
University College Dublin and University of Hohenheim.

Copyright 2003 - 2017 for Basic SKIN and EIS-SKIN: Michel Schilperoord, Nigel
Gilbert, Petra Ahrweiler and Andreas Pyka. All rights reserved.

Permission to use, modify or redistribute this model is hereby granted,
provided that both of the following requirements are followed: a) this
copyright notice is included. b) this model will not be redistributed for
profit without permission and c) the requirements of the Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 Unported License
<http://creativecommons.org/licenses/by-nc-sa/3.0/> are complied with.

The authors of the Basic SKIN and EIS-SKIN models gratefully acknowledge
funding during the course of development of the model from the European
Commission, DAAD, and the British Council.
